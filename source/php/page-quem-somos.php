<?php get_header(); ?>

<section class="conheca">

  <div class="white container px-lg-0 d-lg-flex">

    <div class="col-lg-5 px-0">
      <span class="title-section">Conheça a Diligen</span>

      <div class="line"></div>

      <span class="sub col-9 col-lg-6 px-0">Assessoria em Recuperação Judicial </span>
    </div>

    <div>
      <p>A Diligen atua com serviços de recuperação judicial buscando sempre manter a saúde financeira das empresas e tem o propósito de entregar as melhores soluções possíveis aos clientes, empresas e credores.
      </p>

      <p>Focada em uma área específica, a de disputas judiciais, as atividades abrangem toda a gestão processual por meio de um time de especialistas altamente capacitados para garantir o bom andamento dos serviços.</p>
    </div>

  </div>

  <div class="blue container">

    <img class="estrutura d-none d-lg-block pl-0 " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-somos-desk.png" alt="estrutura Diligen">
    <img class="estrutura d-lg-none pl-0 " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-mob-1.png" alt="estrutura Diligen">

    <p class="col-lg-8 px-0 pr-lg-4">A empresa nasceu da vontade fazer diferente, depois que os três sócios, observarem uma crescente demanda no mercado de recuperação de empresas, impulsionada por diferentes fatores do cenário econômico brasileiro, os sócios Alexandre Temerloglou, Fabio Astrauskas e Guilherme Lopes decidiram então que era o momento ideal para dar início às atividades da Diligen, empresa que já nasceu cheia de credibilidade graças à união das expertises e de anos de experiência dos seus fundadores.
      Ao oferecer uma grande gama de serviços, a Diligen tem como missão dar o melhor suporte e assessoria possível em todas as etapas do processo.</p>



  </div>

</section>

<!-- <section class="equipe container">

  <div class="detail-box col-9 col-lg-7 br-lt"></div>

  <span class="title-section">Equipe</span>

  <div class="person">

    <div class="col-lg-4 px-0">
      <div class="foto alexandre"></div>
      <img class="left " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-person.png" alt="">
    </div>

    <div class="col-lg-8 pl-lg-5 px-0 pr-lg-0">
      <span class="title">Alexandre Temerloglou</span>

      <p>Administrador, Alexandre Temerloglou é especialista em recuperação judicial e extrajudicial de empresas, formado pelo Insper. Com experiência de mais de 20 anos de mercado, já atuou em consultoria de recursos humanos, escritórios de advocacia e hoje atua como Gerente de Projetos na Siegen Consultoria. Ele participou de mais de 50 projetos de recuperação de empresas; dentre eles, mais de 30 planos de recuperação judicial, 100% deles aprovados. Alexandre também é reconhecido por sua forte habilidade em negociação e mediação com bancos, fornecedores, clientes e classes sindicais.</p>
    </div>

  </div>

  <div class="person flex-row-reverse">

    <div class="col-lg-4 px-0">
      <div class="foto guilherme"></div>
      <img class="right right-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-person.png" alt="">
    </div>

    <div class="col-lg-8 pr-lg-5 px-0 pl-lg-0">
      <span class="title">Guilherme Lopes</span>

      <p>Advogado, Guilherme Lopes, é especialista em recuperação de empresas. Atuou em demandas contenciosas com enfoque em procedimentos concursais em favor de devedores e credores. Atuou também, como administrador judicial, tendo coordenado mais de 40 projetos de administração judicial, de recuperações judiciais e falências de empresas dos mais variados setores, sendo algumas delas, de grande repercussão econômica e nacional; é membro do Turnaround Management Association Brasil (TMA Brasil).</p>
    </div>

  </div>

  <div class="person">

    <div class="col-lg-4 px-0">
      <div class="foto fabio"></div>
      <img class="left" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-person.png" alt="">
    </div>

    <div class="col-lg-8 px-0 pl-lg-5 pr-lg-0">
      <span class="title">Fábio Astrauskas</span>

      <p>Engenheiro e economista, com MBA em gestão executiva internacional, Fábio é especialista na recuperação de empresas em crise e participou de processos de reestruturação de várias delas. Mestre em Administração, com foco em planejamento e administração na Recuperação Judicial, é autor do primeiro estudo acadêmico sobre o tema no Brasil. Sua dissertação de mestrado está entre as mais visitadas no site de teses da USP. No governo federal, foi chefe do setor de informática da Divisão de Arrecadação da 8a SRF. É fundador e conselheiro fiscal da TMA Brasil e membro do IBGC - Instituto Brasileiro de Governança Corporativa. Na área acadêmica, foi professor adjunto do Centro Universitário FMU e, atualmente, é professor e coordenador do INSPER para o curso “Turnaround de Empresas: da reestruturação à recuperação”.</p>
    </div>

  </div>

  <a href="<?= get_site_url(); ?>/servicos" class="btn-servicos  col-12 col-lg-11  pr-lg-0">

    <div class="detail-lg-btn">

      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-hand.png" alt="">

    </div>

    <div class="d-flex align-items-center col-10 col-lg-7 px-0">
      <span class=" col-lg-11 pl-3 pr-0">Conheça todos os nossos serviços</span>

      <span class="arrow">></span>
    </div>


  </a>

</section> -->

<?= get_template_part('porque'); ?>

<?= get_template_part('pre-footer'); ?>

<?php get_footer(); ?>