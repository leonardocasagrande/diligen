<div class="porque container <?php if (is_page('servicos')) : ?>  mt-0 <?php endif; ?>">

  <div class="padding-mobile">
    <img class="d-color d-lg-none <?php if (is_page(array('quem-somos', 'faq', 'servicos'))) : ?> black <?php endif ?>" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/d-brown.png" alt="">

  </div>

  <img class="d-color d-none d-lg-block <?php if (is_page(array('quem-somos', 'faq', 'servicos'))) : ?> black <?php endif ?>" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/d-porque-desk.png" alt="">

  <div class="position-custom">
    <span class="title-section text-center text-lg-left">Por que a Diligen?</span>

    <p class="col-lg-7 px-0"><b>Redução de Gastos Operacionais:</b> A contratação de um assessor terceirizado permite manter uma estrutura operacional reduzida e ampliar resultados.</p>

    <p class="col-lg-7 px-0"><b>Expertise Econômico-Financeira:</b> Uso de profissionais multidisciplinares com experiência contábil, econômica e financeira aplicados ao contexto da recuperação empresarial, valuation e due diligence.</p>

    <p class=""><b>Foco em questões jurídicas:</b> A possibilidade de utilização de um assessor permite o foco em questões jurídicas.
    </p>

    <p class=""><b>Expertise em conciliação e mediação:</b> Uso de profissionais altamente gabaritados para situações de conflitos e que necessitam a figura de um mediador para alcançar uma conciliação.
    </p>
  </div>

</div>