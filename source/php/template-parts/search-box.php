<?php if (is_page('processos')) : $search_processos = 'search-processos';
endif; ?>

<?php if (is_single()) :  $top_30 = 'top-30';
endif; ?>


<div class="search-component <?php echo $search_processos, $top_30;  ?>">
  <span class="text px-3">Encontre informações sobre nossos processos:</span>

  <div class="box col-lg-8">
    <?php get_search_form() ?>
  </div>
</div>

</div>