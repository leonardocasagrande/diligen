<?php get_header() ?>

<section class="processos-content container">
  <?php
  $paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1 );
  $s=get_search_query();
  $args = array(
	  'posts_per_page' => 6,
	  'post_type' => 'processo',
	  's' => $s,
	  'paged' => $paged,
  );
  $processos = new WP_Query($args);
  if ($processos->have_posts()) : while ($processos->have_posts()) : $processos->the_post(); 
  $titulo = get_the_title();
  $img = get_field('miniatura_na_busca');
  $link = get_permalink();
  if(!$img){
    $img = get_stylesheet_directory_uri()."/dist/img/d-mini.png";
  }
  ?>
  <div class="item d-lg-flex align-items-center">

    <div class="header ">

      <div class="border-lg col-lg-4 col-2 px-0">
        <img class="" src="<?= $img; ?>" alt="">
      </div>

      <span class="title col-8 col-lg-12 pb-0"><?= $titulo; ?></span>

    </div>

    <a href="<?= $link; ?>" class="btn-saiba">Saiba mais ></a>

  </div>
  <?php
  endwhile;  
  ?>
  <div class="paginacao">
		<?php include('paginacao.php');?>
	</div>
	<?php 
	else:
		echo "<span class='no-result'>Nenhum processo para essa busca foi publicado ainda</span>";
	?>
  <?php endif; ?>
</section>

<section class="processos-envio container d-lg-none">

  <span class="title">Formulário</span>

  <?= do_shortcode('[contact-form-7 id="72" title="Envio de Documentos"]'); ?>

</section>


<section class="btn-page">

  <div class="btn-box container">


    <a href="<?= get_site_url(); ?>/servicos" class="btn-servicos  col-12 pr-lg-0">

      <div class="detail-lg-btn">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-hand.png" alt="">

      </div>

      <div class="d-flex col-10 col-lg-7 px-0">
        <span class=" col-lg-11 pl-3 pr-0">Conheça todos os nossos serviços</span>

        <span class="arrow">></span>
      </div>


    </a>

  </div>

</section>


<?php get_footer() ?>