<?php get_header(); ?>

<section class="faq container  ">

  <span class="title-section">1. Crédito</span>

  <div id="accordion">
    <div class="card">
      <div class="card-header" id="headingOne">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <span class="title col-11 px-0">1.1 Fui comunicado pelo Administrador Judicial que sou credor de uma empresa em Recupera-ção Judicial e eu concordo com o valor e a classificação do crédito informado. O que devo fazer?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>

      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
          <p>Se as informações estão corretas, significa que o seu crédito está devidamente habilitado. É fortemente recomendado de acompanhar o andamento do processo para saber como será pago o valor devido, as-sim como participar de eventuais assembleias de credores.</p>


        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <span class="title col-11 px-0">1.2 Fui comunicado pelo Administrador Judicial que sou credor de uma empresa em Recupera-ção Judicial e eu não concordo com o valor e/ou a classificação do crédito informado. O que posso fazer?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">
          <p>1) Se o valor está incorreto, será necessário solicitar a retificação ao Administrador Judicial, via o email, informando na carta com o assunto: Divergência de crédito + Nome do credor, ou pelo correio no endereço, Rua XXXXXXXXX. A apresentação de divergência deve ser apresentada no prazo de 15 (quinze) dias corridos a partir da publicação do 1º edital (art. 7, § 1º da Lei 11.101/2005).[Habilitação / Divergência de crédito: Fase administrativa]</p>

          <p>A deliberação acerca de todas as divergências e habilitação de créditos, será apresentado pelo Administrador Judicial no 2º edital (art. 7, § 2º da Lei 11.101/2005), no prazo de 45 dias corri-dos contados do término do prazo de habilitação.
          </p>

          <p>B)2) Se você discordar do valor apurado pelo Administrador Judicial no 2º edital, poderá ainda discutir sobre o mérito do seu crédito no prazo de 10 (dez) dias corridos, contado da publicação do 2º edital (art. 8, da Lei 11.101/2005). Neste caso, você precisará enviar a sua solicitação diretamente ao juízo da Recuperação Judicial (art. 13 da Lei 11.101/2005), por meio de um incidente proces-sual e ser representado por um advogado. [Habilitação / Impugnação de crédito: Fase judicial] </p>

          <p>Findado o prazo acima de impugnação, você será intimado a contestar o crédito no prazo de 5 (cinco) dias corridos juntando todos os documentos e provas (art. 11, da Lei 11.101/2005). No prazo de 10 dias corridos após o prazo mencionado acima, será apresentado pelo Adminis-trador Judicial um quadro-geral de credores (art. 18, § único da Lei 11.101/2005), contando as decisões proferidas sobre as impugnações apresentadas.
          </p>

        </div>
      </div>

      <div class="line"></div>

    </div>



    <div class="card">
      <div class="card-header" id="headingThree">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <span class="title col-11 px-0">1.3 Fui comunicado pelo Administrador Judicial que sou credor de uma empresa em Recupera-ção Judicial e eu não concordo com o valor e/ou a classificação do crédito informado e perdi o prazo de 15 dias para promover a minha divergência de crédito. O que posso fazer?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
        <div class="card-body">
          <p>1) Você ainda terá 10 (dez) dias para apresentar a sua divergência de crédito a partir da publicação do 2º edital do Administrador Judicial. Neste caso, você precisará enviar a sua solicitação direta-mente ao juízo da Recuperação Judicial e ser representado por um advogado. [Habilitação / Impugnação de crédito: Fase judicial]
          </p>

          <p>2) Se você perdeu o prazo para apresentar a sua divergência depois da publicação do 2º edital, você poderá fazê-lo enviando diretamente ao juízo da Recuperação Judicial.</p>
        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingFour">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
            <span class="title col-11 px-0">1.4 Sou credor de uma empresa em Recuperação Judicial e o meu crédito não foi relacionado no processo. Como posso habilitar o meu crédito?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
        <div class="card-body">
          <p> 1) Será necessário comunicar ao Administrador Judicial, via email, informando na carta com o as-sunto: Habilitação de crédito + Nome do credor. A apresentação de habilitação de crédito deve ser apresentada no prazo de 15 (quinze) dias corridos a partir da publicação do 1º edital (art. 7, § 1º da Lei 11.101/2005).
            [Habilitação / Divergência de crédito: Fase administrativa]
          </p>

          <p>A deliberação acerca de todas as divergências e habilitação de créditos, será apresentado pelo Administrador Judicial no 2º edital (art. 7, § 2º da Lei 11.101/2005), no prazo de 45 dias corridos contados do término do prazo de habilitação.</p>

          <p>2) Se você perdeu o prazo mencionado acima, você poderá ainda se habilitar na qualidade de credor retardatário no prazo de 10 (dez) dias corridos, contado da publicação do 2º edital (art. 8, da Lei 11.101/2005). Neste caso, você precisará enviar a sua solicitação diretamente ao juízo da Recupe-ração Judicial (art. 13 da Lei 11.101/2005), por meio de um incidente processual e ser represen-tado por um advogado.
            [Habilitação / Impugnação de crédito: Fase judicial]
          </p>

          <p>Findado o prazo acima de impugnação, você será intimado a contestar o crédito no prazo de 5 (cinco) dias corridos juntando todos os documentos e provas (art. 11, da Lei 11.101/2005).
            No prazo de 10 dias corridos após o prazo mencionado acima, será apresentado pelo Administra-dor Judicial um quadro-geral de credores (art. 18, § único da Lei 11.101/2005), contando as deci-sões proferidas sobre as impugnações apresentadas.
          </p>

          <p>3) Se você ainda não fez o pedido de habilitação após a publicação do quadro-geral de credores, você precisará enviar a sua solicitação diretamente ao juízo da Recuperação Judicial (art. 13 da Lei 11.101/2005).</p>



        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingFive">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
            <span class="title col-11 px-0">1.5 Sou credor de uma empresa que foi convolada em falência e o meu crédito não foi relacio-nado no processo. Como posso habilitar o meu crédito?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
        <div class="card-body">

          <p> 1) Será necessário comunicar ao Administrador Judicial, via email, informando na carta com o as-sunto: Habilitação de crédito + Nome do credor. A apresentação de habilitação de crédito deve ser apresentada no prazo de 15 (quinze) dias corridos a partir da publicação do 1º edital (art. 7, § 1º da Lei 11.101/2005).
            [Habilitação / Divergência de crédito: Fase administrativa]
          </p>

          <p>A deliberação acerca de todas as divergências e habilitação de créditos, será apresentado pelo Administrador Judicial no 2º edital (art. 7, § 2º da Lei 11.101/2005), no prazo de 45 dias corridos contados do término do prazo de habilitação.</p>

          <p>2) Se você perdeu o prazo mencionado acima, você poderá ainda se habilitar na qualidade de credor retardatário no prazo de 10 (dez) dias corridos, contado da publicação do 2º edital (art. 8, da Lei 11.101/2005). Neste caso, você precisará enviar a sua solicitação diretamente ao juízo da Recupe-ração Judicial (art. 13 da Lei 11.101/2005), por meio de um incidente processual e ser represen-tado por um advogado.
            [Habilitação / Impugnação de crédito: Fase judicial]
          </p>

          <p>Findado o prazo acima de impugnação, você será intimado a contestar o crédito no prazo de 5 (cinco) dias corridos juntando todos os documentos e provas (art. 11, da Lei 11.101/2005).
            No prazo de 10 dias corridos após o prazo mencionado acima, será apresentado pelo Administra-dor Judicial um quadro-geral de credores (art. 18, § único da Lei 11.101/2005), contando as deci-sões proferidas sobre as impugnações apresentadas.
          </p>

          <p>3) Se você ainda não fez o pedido de habilitação após a publicação do quadro-geral de credores, você precisará enviar a sua solicitação diretamente ao juízo da Recuperação Judicial (art. 13 da Lei 11.101/2005).</p>

        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingSix">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
            <span class="title col-11 px-0">1.6 O que devo apresentar em uma habilitação ou divergência de crédito?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
        <div class="card-body">
          <p>Conforme Art. 9 da Lei 11.101/2005, a solicitação deverá conter:</p>
          <p>I – o nome, o endereço do credor e o endereço em que receberá comunicação de qualquer ato do processo;</p>
          <p>II – o valor do crédito, atualizado até a data da decretação da falência ou do pedido de recuperação judicial, sua origem e classificação;</p>
          <p>III – os documentos comprobatórios do crédito e a indicação das demais provas a serem produzidas;</p>
          <p>IV – a indicação da garantia prestada pelo devedor, se houver, e o respectivo instrumento;</p>
          <p>V – a especificação do objeto da garantia que estiver na posse do credor.</p>
          <p>Parágrafo único. Os títulos e documentos que legitimam os créditos deverão ser exibidos no original ou por cópias autenticadas se estiverem juntados em outro processo.</p>
        </div>
      </div>

      <div class="line"></div>

    </div>

    <div class="card">
      <div class="card-header" id="headingcredito1">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#Credit1" aria-expanded="false" aria-controls="Credit1">
            <span class="title col-11 px-0">1.7 Qual é a data corte para saber se o crédito que detenho com a empresa devedora está rela-cionado com a Recuperação Judicial?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="Credit1" class="collapse" aria-labelledby="headingcredito1" data-parent="#accordion">
        <div class="card-body">
          <p>A data corte que determine se o credor está sujeito ou não aos efeitos da Recuperação Judicial é a data do pedido da Recuperação Judicial.</p>

          <p>Antes da data do pedido de Recuperação Judicial ele está sujeito, depois da data, ele deve ser pago con-forme acordado entre as partes.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingcredito2">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#Credit2" aria-expanded="false" aria-controls="Credit2">
            <span class="title col-11 px-0">1.8 Qual é a data para atualizar o crédito que detenho com a empresa devedora?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="Credit2" class="collapse" aria-labelledby="headingcredito2" data-parent="#accordion">
        <div class="card-body">
          <p>Todos os débitos devem ser atualizados até a data do pedido da Recuperação Judicial.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingcredito3">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#Credit3" aria-expanded="false" aria-controls="Credit3">
            <span class="title col-11 px-0">1.9 Como saber se o crédito que detenho entra ou não na Recuperação Judicial?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="Credit3" class="collapse" aria-labelledby="headingcredito3" data-parent="#accordion">
        <div class="card-body">
          <p> Trabalhista: Se você tinha um vínculo empregatício e foi demitido e a empresa devedora não pagou a rescisão, o acordo, o saldo de acordo ou o processo trabalhista, o fato gerador é a data de demissão. Se a demissão aconteceu antes do pedido de Recuperação Judicial, o crédito deve ser elencado na lista de credores.</p>
          <p> Fornecedor (produto): Se você vendeu um produto e não recebeu o valor, o fato gerador é a emis-são da data na Nota Fiscal. Se o fato gerador aconteceu antes do pedido de Recuperação Judicial, o crédito deve ser elencado na lista de credores.</p>

          <p> Fornecedor (serviço): Se você prestou um serviço e não recebeu o valor, o fato gerador é a data da prestação do serviço. Se o fato gerador aconteceu antes do pedido de Recuperação Judicial, o crédi-to deve ser elencado na lista de credores.</p>

          <p>Se o seu crédito está arrolado ou garantido em umas das modalidades abaixo, ele não entra na Re-cuperação Judicial:</p>

          <ul>
            <li> Alienação Fiduciária</li>
            <li> Cessão Fiduciária</li>
            <li> ACC</li>
            <li> ACE</li>
            <li> Créditos garantidos por terceiros</li>
            <li> Créditos oriundos de arrendamento mercantil</li>
            <li> Operação Imobiliárias com clausula de irrevogabilidade, irretratabilidade ou reserva de domínio</li>
          </ul>

        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingcredito4">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#Credit4" aria-expanded="false" aria-controls="Credit4">
            <span class="title col-11 px-0">1.10 Como e quando receberei o meu crédito na Recuperação Judicial?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="Credit4" class="collapse" aria-labelledby="headingcredito4" data-parent="#accordion">
        <div class="card-body">
          <p>Os pagamentos ocorrerão conforme previsto no plano de recuperação judicial ou no seu último aditivo que foi homologado pelo Juiz.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingcredito5">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#Credit5" aria-expanded="false" aria-controls="Credit5">
            <span class="title col-11 px-0">1.11 Como e quando receberei o meu crédito na Falência?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="Credit5" class="collapse" aria-labelledby="headingcredito5" data-parent="#accordion">
        <div class="card-body">
          <p>Os créditos serão pagos depois de apurar o passivo total e arrecadar o valor proveniente da venda dos ativos da falida. A ordem dos pagamentos seguirá os critérios estabelecido na lei.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingcredito6">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#Credit6" aria-expanded="false" aria-controls="Credit6">
            <span class="title col-11 px-0">1.12 Meu crédito está em moeda estrangeira, qual será o valor elencado?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="Credit6" class="collapse" aria-labelledby="headingcredito6" data-parent="#accordion">
        <div class="card-body">
          <p>Para fins de votação, o crédito será convertido para moeda nacional pelo câmbio da véspera da data de realização da assembleia. (art. 38 § único)</p>

        </div>
      </div>

      <div class="line"></div>

    </div>

  </div>

  <span class="title-section pt-5 pr-1">2. Plano</span>

  <div id="accordion">
    <div class="card">
      <div class="card-header" id="headingSeven">
        <h5 class="mb-0">
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
            <span class="title col-11 px-0">2.1 Quem elabora o Plano de Recuperação Judicial?</span>
            <span class="icon ">></span>

          </button>
        </h5>
      </div>

      <div id="collapseSeven" class="collapse " aria-labelledby="headingSeven" data-parent="#accordion">
        <div class="card-body">

          <p>A empresa devedora ou uma consultoria contratada por ela. O Administrador Judicial não redige o plano, ele atua como fiscal do juízo.</p>


        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingEight">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
            <span class="title col-11 px-0">2.2 Não concordo com o plano de recuperação judicial. O que posso fazer?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
        <div class="card-body">
          <p>O credor poderá apresentar uma objeção ao plano de recuperação judicial no prazo de 30 dias a contar da publicação do plano. Neste caso, será necessário contratar um advogado.</p>
        </div>
      </div>

      <div class="line"></div>

    </div>



    <div class="card">
      <div class="card-header" id="headingNine">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
            <span class="title col-11 px-0">2.3 Se o Plano for aprovado ou reprovado pela maioria dos credores, o que acontece?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
        <div class="card-body">
          <p>Se o plano for aprovado, será concedido a Recuperação Judicial e empresa devedora terá que cumprir com os termos do seu Plano de Recuperação.</p>
          <p>Se o plano for reprovado, poderá ser convolada a Recuperação Judicial em falência.
          </p>
        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingTen">
        <h5 class="mb-0">
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
            <span class="title col-11 px-0">2.4 Como serão realizadas as intimações aos credores?</span>
            <span class="icon ">></span>

          </button>
        </h5>
      </div>

      <div id="collapseTen" class="collapse " aria-labelledby="headingTen" data-parent="#accordion">
        <div class="card-body">

          <p>As intimações serão efeituadas via edital, tanto na Recuperação Judicial como na Falência</p>


        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingEleven">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
            <span class="title col-11 px-0">2.5 O que devo fazer para votar na Assembleia Geral de Credores?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
        <div class="card-body">

          <p>A1) Se for uma pessoa física, deverá comparecer com um documento de identidade (art. 37 § 4º).</p>

          <p>A2) Se for uma pessoa jurídica, deverá comparecer com um documento de identidade e com uma cópia do contrato social atestando que ele tem poder de representação ou a indicação das folhas dos autos do processo em que se encontre o documento (art. 37 § 4º).
          </p>


        </div>
      </div>

      <div class="line"></div>

    </div>



    <div class="card">
      <div class="card-header" id="headingTwelve">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
            <span class="title col-11 px-0">
              2.6 Alguém pode me representar na Assembleia Geral de Credores?
            </span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
        <div class="card-body">

          <p>Se for representado por um terceiro, este deverá entregar ao Administrador Judicial, uma procuração ou indicação das folhas dos autos onde se encontra esta procuração. Esses documentos devem ser entregues em até 24h antes da data da assembleia. (art. 37 § 5º).</p>

          <p>Se for os sindicatos de trabalhadores, deverão apresentar ao Administrador Judicial, a relação de seus associados que representarão, com a respectiva procuração que lhes foram outorgadas. Esses documentos devem ser entregues em até 10 dias antes da assembleia. (art. 37 § 6).</p>



        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingThirteen">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
            <span class="title col-11 px-0">2.7 O Plano será votado na 1ª Assembleia Geral de Credores instalada?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
        <div class="card-body">

          <p>A votação acontecera na primeira Assembleia Geral de Credores instalada, caso for a vontade da maioria dos credores por valores presentes (art 42). Caso contrário, a Assembleia Geral de Credores será suspen-dida e será marcada uma nova data para possível votação.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingFourteen">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
            <span class="title col-11 px-0">2.8 Não participei da 1ª e 2ª chamadas da Assembleia Geral de Credores, posso ainda comparecer para votar nas prorrogações da 2ª chamada?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordion">
        <div class="card-body">
          <p>Só poderão ter poder de voto, os credores que participaram da primeira Assembleia Geral de Credores instalada. Se não compareceu, ainda poderá participar, porém, apenas como ouvinte.</p>

          <p>A Assembleia Geral de Credores é instalada na primeira convocação se estiver presente mais da metade dos credores em termos de valores por cada classe. Se isso não acontecer, será convocado uma 2ª As-sembleia Geral de Credores que será instalada independentemente do número de credores presentes. (art. 37 § 2).</p>


        </div>
      </div>

      <div class="line"></div>

    </div>
    <div class="card">
      <div class="card-header" id="headingFifteen">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
            <span class="title col-11 px-0">2.9 Não consegui participar de algumas das Assembleia Geral de Credores, posso participar nas próximas?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen" data-parent="#accordion">
        <div class="card-body">
          <p>Só poderá participar Assembleia Geral de Credores com poder de voto, o credor que for presente na As-sembleia Geral de Credores que foi instalada. Caso contrário, só poderá participar como ouvinte.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>

  </div>



  <div class="card">
    <div class="card-header" id="headingSixteen">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
          <span class="title col-11 px-0">2.10 Uma empresa me perguntou se eu queria vender o meu crédito, isso é permitido?</span>
          <span class="icon ">></span>

        </button>
      </h5>
    </div>

    <div id="collapseSixteen" class="collapse " aria-labelledby="headingSixteen" data-parent="#accordion">
      <div class="card-body">

        <p>Sim, você poderá transferir o seu crédito para um terceiro e ele a passará a ser o credor em seu lugar. Observe com o seu advogado como proceder à cessão do crédito.</p>

      </div>
    </div>

    <div class="line"></div>

  </div>

  <span class="title-section pt-5">3. Função do AJ</span>

  <div id="accordion ">


    <div class="card mb-5">
      <div class="card-header" id="headingSeventeen">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
            <span class="title col-11 px-0">3.1 Qual é a função do Administrador Judicial</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseSeventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordion">
        <div class="card-body">

          <p>Na Recuperação Judicial, o Administrador Judicial é uma auxiliar do juíz e não representa e nem tem poderes de gestão sobre a Recuperanda. O seu dever é fiscalizar as atividades da recuperanda, presidir a Assembleia geral de credores e verificar o comprimento do plano.</p>

          <p>Na Falência, o Adminstrador Judicial representa a massa falida e é responsável para alienar os ativos da empresa para arrecadar e pagar os credores conforme ordem prevista em lei.</p>


        </div>
      </div>

      <div class="line"></div>

    </div>


  </div>


  <!-- <span class="title-section pt-5">Outras dúvidas</span>

  <div id="accordion">
    <div class="card">
      <div class="card-header" id="headingNineteen">
        <h5 class="mb-0">
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseNineteen" aria-expanded="false" aria-controls="collapseNineteen">
            <span class="title col-11 px-0">Não concordo com o plano de recuperação judicial. O que posso fazer?</span>
            <span class="icon ">></span>

          </button>
        </h5>
      </div>

      <div id="collapseNineteen" class="collapse " aria-labelledby="headingNineteen" data-parent="#accordion">
        <div class="card-body">
          <p>O credor poderá apresentar uma objeção ao plano de recuperação judicial no prazo de 30 dias a contar da publicação do plano. Nesse caso será necessário contratar um advogado.</p>

        </div>
      </div>

      <div class="line"></div>

    </div>


    <div class="card">
      <div class="card-header" id="headingTwenty">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwenty" aria-expanded="false" aria-controls="collapseTwenty">
            <span class="title col-11 px-0">Como serão realizadas as intimações aos credores?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseTwenty" class="collapse" aria-labelledby="headingTwenty" data-parent="#accordion">
        <div class="card-body">
          <p>As intimações serão efetuadas via edital, tanto na Recuperação Judicial como na Falência</p>
        </div>
      </div>

      <div class="line"></div>

    </div>



    <div class="card">
      <div class="card-header" id="headingTwentyOne">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwentyOne" aria-expanded="false" aria-controls="collapseTwentyOne">
            <span class="title col-11 px-0">O que devo fazer para votar na Assembleia de Credores?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseTwentyOne" class="collapse" aria-labelledby="headingTwentyOne" data-parent="#accordion">
        <div class="card-body">
          <p> A1) Se for uma pessoa física, deverá comparecer pessoalmente com um documento de identidade</p>

          <p> A2) Se for uma pessoa jurídica, deverá comparecer pessoalmente com um documento de identidade e com uma cópia do contrato social, atestando que ele tem poder de representação.</p>

          <p>B) Se for representado por um terceiro, ele deverá entregar ao Administrador Judicial, uma procuração ou indicação das folhas dos autos onde se encontra essa procuração. Esses documentos devem ser entregues em até 24h antes da data da assembleia.</p>

          <p> C) Se forem os sindicatos de trabalhadores, deverão apresentar ao Administrador Judicial a relação de seus associados que representarão, com a respectiva procuração que lhes foram outorgadas. Esses documentos devem ser entregue em até 10 dias antes da assembleia</p>

        </div>
      </div>

      <div class="line"></div>

    </div>

    <div class="card">
      <div class="card-header" id="headingTwentyTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwentyTwo" aria-expanded="false" aria-controls="collapseTwentyTwo">
            <span class="title col-11 px-0">Qual é a função do Administrador Judicial?</span>
            <span class="icon">></span>

          </button>
        </h5>
      </div>
      <div id="collapseTwentyTwo" class="collapse" aria-labelledby="headingTwentyTwo" data-parent="#accordion">
        <div class="card-body">
          <p>Na Recuperação Judicial, o Administrador Judicial é uma auxiliar do juiz e não representa e nem tem poderes de gestão sobre a Recuperanda. O seu dever é fiscalizar as atividades da Recuperanda, presidir a Assembleia Geral de credores e verificar o comprimento do plano.</p>

          <p>Na Falência, o Administrador Judicial representa a massa falida e é responsável por alienar os ativos da empresa para arrecadar e pagar os credores, conforme ordem prevista em lei.
          </p>
        </div>
      </div>

      <div class="line"></div>

    </div>




  </div>

</section> -->

  <section class="duvida container-lg  position-relative">

    <div class="box-blue br-bl container col-lg-6">

      <div class="detail-box col-11 col-lg-8 br-lt"></div>

      <span class="title col-lg-9 px-0">Ainda tem alguma dúvida?</span>

      <p class="col-lg-11 px-0">Preencha o formulário com todas as suas informações para receber o contato da nossa equipe.</p>

    </div>

    <div class="box-brown col-11 col-lg-6">

      <?php echo do_shortcode('[contact-form-7 id="71" title="FAQ"]'); ?>

      <!-- <input type="text" placeholder="Nome">

    <input type="text" placeholder="E-mail">

    <input type="text" placeholder="Assunto">

    <textarea name="" placeholder="Mensagem" id="" cols="30" rows="10"></textarea>

    <div class="btn col-4">enviar</div> -->

    </div>


  </section>

  <?= get_template_part('porque'); ?>

  <?= get_template_part('pre-footer'); ?>

  <?php get_footer(); ?>