<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-G7CMPK6C8Z"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-G7CMPK6C8Z');
  </script>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title> <?= wp_title(); ?></title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

</head>

<body>

  <div class="invisivel d-lg-none"></div>

  <header class="d-lg-block d-none  ">

    <div class="container">

      <a href="<?= get_site_url(); ?>/">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">
      </a>

      <div class="links">

        <a <?php if (is_page('quem-somos')) : echo "class='page-active'";
            endif; ?> href="<?= get_site_url(); ?>/quem-somos">Quem somos</a>
        <a <?php if (is_page('servicos')) : echo "class='page-active'";
            endif; ?> href="<?= get_site_url(); ?>/servicos">serviços</a>
        <a <?php if (is_page('faq')) : echo "class='page-active'";
            endif; ?> href="<?= get_site_url(); ?>/faq">faq</a>
        <a <?php if (is_page('processos')) : echo "class='page-active'";
            endif; ?> href="<?= get_site_url(); ?>/processos">processos</a>
        <a <?php if (is_page('envio-de-documentos')) : echo "class='page-active'";
            endif; ?> href="<?= get_site_url(); ?>/envio-de-documentos">envio de documentos</a>
        <a <?php if (is_page('contato')) : echo "class='page-active'";
            endif; ?> href="<?= get_site_url(); ?>/contato">contato</a>

      </div>


    </div>



  </header>


  <nav class=" d-lg-none top-nav container " id="top-nav">

    <a href="<?= get_site_url(); ?>/">
      <div class="logo-nav"></div>
    </a>

    <input class="menu-btn" type="checkbox" id="menu-btn" />

    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>


    <div class="menu">

      <a href="<?= get_site_url(); ?>/quem-somos">Quem somos</a>
      <a href="<?= get_site_url(); ?>/servicos">serviços</a>
      <a href="<?= get_site_url(); ?>/faq">faq</a>
      <a href="<?= get_site_url(); ?>/processos">processos</a>
      <a href="<?= get_site_url(); ?>/envio-de-documentos">envio de documentos</a>
      <a href="<?= get_site_url(); ?>/contato">contato</a>

      <a href="https://www.linkedin.com/company/diligen-aj/" target="_blank" class="mini">
        <span>Siga-nos no Linkedin</span>
        <i class="fab fa-linkedin-in"></i>
      </a>
    </div>
  </nav>



  <?php if (is_singular('servico')) : $banner_single = 'banner-single';
  endif; ?>

  <?php if (is_page(
    array('faq', 'servicos', 'quem-somos', 'envio-de-documentos', 'contato')
  )) :
    $banner_2 = 'banner-2';
  endif; ?>
  <?php if (is_singular('processo')) $banner_processo = 'banner-processo'; ?>

  <?php if (is_page('processos')) : $banner_processo = 'banner-processo banner-proc-2';

  endif ?>


  <div class=" banner  <?= $banner_single, $banner_processo, $banner_2, $banner_3; ?> ">
    <div class="container">

      <span class="page-name px-0  "><?= the_title(); ?></span>

      <span class="name-bold">Diligen:</span>
      <span class="title-banner col-11 col-lg-12 px-0 d-block ">A solução Siegen para o Poder Judiciário e Administradores Judiciais</span>

      <p class="col-lg-9 px-0">Formada por profissionais da Siegen focados na área de insolvência, a Diligen é a solução na área econômico-financeira <b>para administradores judiciais, valuation e due diligence</b>, além de atuar com mediação e conciliação, agindo com transparência, seriedade e comprometimento.</p>

      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/d-banner-desk.png" alt="" class=" d-desk">


      <div class="line"></div>

      <span class="name-single"><?= the_title(); ?></span>

      <span class="name-processo"><?= the_title(); ?></span>

      <span class="processo-texto px-0 col-lg-8">Para acessar as informações sobre um processo de Recuperação Judicial ou Falência específico, digite no campo abaixo o nome da empresa.</span>


      <?= get_template_part('search-box'); ?>

    </div>
  </div>