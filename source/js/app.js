$('.menu-icon').on('click', function () {
  $('.top-nav').toggleClass('ativo');
})


$('#accordion button').on('click', function () {

  $('.icon').removeClass('icon-up');

  var isOpen = $(this).attr('aria-expanded');
  let element = this.childNodes[2];

  element.classList.add('icon-up');


  if (isOpen === 'true') {

    element.classList.remove('icon-up');

  }
})

$(document).ready(function(){
  
  var select = $('select.list-processos');
  
  $.each(processos,function(key, value){
    
    select.append("<option value='" + value + "'>" + value + "</option>")
  })
})


function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$('.e-mail').attr('onChange', "validateEmail(this)")


$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});



var sliderServicos = tns({
  container: '.carousel-servicos',
  slideBy: 1,
  autoplay: false,
  loop: false,
  controls: false,
  autoplayButtonOutput: false,
  responsive: {
    200: {
      items: 1.4,
    },
    1000: {
      disable: true
    }
  }

});