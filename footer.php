<footer class="footer"><div class="container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png" alt="diligen logo"><div class="d-lg-flex justify-content-between"><div class="links"><div><a href="<?= get_site_url(); ?>/quem-somos">quem somos</a> <a href="<?= get_site_url(); ?>/servicos">serviços</a> <a href="<?= get_site_url(); ?>/faq">faq</a></div><div><a href="<?= get_site_url(); ?>/processos">processos</a> <a href="<?= get_site_url(); ?>/envio-de-documentos">envio de documentos</a> <a href="<?= get_site_url(); ?>/contato">contato</a></div></div><div class="line"></div><div class="contato-footer col-lg-5 px-lg-0"><a target="_blank" href="https://www.google.com.br/maps/dir//Alameda+Rio+Negro,+1030+-+Escrit%C3%B3rio+206+-+Alphaville+Industrial,+Barueri+-+SP,+06454-000/@-23.4969501,-46.8479121,20z/data=!4m8!4m7!1m0!1m5!1m1!1s0x94cf023cc50b1615:0x8856b04b912937d0!2m2!1d-46.8478281!2d-23.4969374"><span>Alameda Rio Negro, 1030 - Escritório 206<br>Condomínio Stadium,<br>CEP: 06454-000<br>Barueri/SP</span></a><div class="contato-flex"><a href="tel:+551148100861"><i class="fa fa-phone-alt"></i><span>(11) 4810-0861</span></a> <a href="https://www.linkedin.com/company/diligen-aj/" target="_blank"><i class="fab fa-linkedin-in"></i><span>Siga-nos no Linkedin</span></a></div></div></div></div></footer> <?php wp_footer(); ?> <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script><script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script><script> <?php
  $args = array(
    'post_type' =>  'processo',
    'posts_per_page' => -1,
    'order' => 'ASC',
  );
  $listprocessos = array();
  $processos = new WP_Query($args);
  if ($processos->have_posts()) : while ($processos->have_posts()) : $processos->the_post();

      array_push($listprocessos, get_the_title());
    endwhile;
  endif;
  // var_dump($listprocessos);
  $listprocessos = json_encode($listprocessos);
  ?> processos = <?= $listprocessos ?>;</script><script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>