<?php


function my_function_admin_bar()
{
	return false;
}
add_filter('show_admin_bar', 'my_function_admin_bar');

function cptui_register_my_cpts_servico()
{

	/**
	 * Post Type: Serviço.
	 */

	$labels = [
		"name" => __("Serviços", "custom-post-type-ui"),
		"singular_name" => __("Serviço", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Serviço", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "servico", "with_front" => true],
		"query_var" => true,
		"menu_icon" => "http://localhost/diligen/wp-content/uploads/2020/12/oie_transparent-1.png",
		"supports" => ["title", "excerpt", "custom-fields"],
	];

	register_post_type("servico", $args);
}

add_action('init', 'cptui_register_my_cpts_servico');
function cptui_register_my_cpts_processo() {

	/**
	 * Post Type: Processos.
	 */

	$labels = [
		"name" => __( "Processos", "custom-post-type-ui" ),
		"singular_name" => __( "Processo", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Processos", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "processo", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "processo", $args );
}

add_action( 'init', 'cptui_register_my_cpts_processo' );

add_theme_support( 'post-thumbnails' );
?>