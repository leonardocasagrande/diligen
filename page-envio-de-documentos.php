<?php get_header(); ?> <section class="envio-docs container-lg"><div class="container white"><p class="col-lg-10 px-0">Faça o upload de toda a documentação pertinente ao processo de um jeito simples e garanta que os trâmites jurídicos estejam sempre atualizados e sem prejuízo no andamento. Não esqueça de preencher todos os campos.</p></div><div class="form-box container"><span class="title">Formulário</span> <?php echo do_shortcode('[contact-form-7 id="72" title="Envio de Documentos"]'); ?> <!-- <input class="col-12" type="text" placeholder="Nome">

    <div class="d-lg-flex justify-content-between">
      <input class="col-12 w-49 " type="text" placeholder="E-mail">

      <input class="col-12 w-49" type="text" placeholder="Telefone">
    </div>

    <select class="col-12" name="" id="">
      <option value="" disabled selected>Processo</option>
      <option value="">Processo 2</option>
      <option value="">Processo 3</option>
    </select>

    <input class="col-12" type="file" name="" id="" placeholder="Selecione um arquivo..">

    <textarea class="col-12" name="" placeholder="Mensagem" id="" cols="30" rows="10"></textarea>

    <a href="#" class="btn col-4 col-lg-2">Enviar</a> --></div><div class="btn-box"><a href="<?= get_site_url(); ?>/processos" class="btn-servicos col-11 px-lg-0"><div class="detail-lg-btn"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-folder.png" alt=""></div><div class="d-flex col-10 col-lg-7 px-0"><span class="col-lg-11 pl-3 pr-0 pt-1">Confira todos os nossos processos</span> <span class="arrow">></span></div></a></div></section> <?php get_footer(); ?>