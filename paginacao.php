<div class="barradenavegacao"> <?php
    echo paginate_links( array(
        'format' => '?pagina=%#%',
        'show_all'  => false,
        'current' => max( 1, $paged ),
        'total' => $processos->max_num_pages,
        'prev_text' => '<',
        'next_text' => '>',
        'type'      => 'list'
    ) );
    
    ?> </div>