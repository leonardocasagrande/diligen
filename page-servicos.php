<?= get_header(); ?> <section class="servicos"><div class="container"> <?php
    $contador = 1;

    $argsServicos = array(
      'post_type' => 'servico',
      'orderby' => array(
        'date' => 'ASC'
      )
    );
    $Servicos = new WP_Query($argsServicos);
    if ($Servicos->have_posts()) :
      while ($Servicos->have_posts()) : $Servicos->the_post();
    ?> <?php if ($contador == 1) : ?> <div class="item col-12 px-0"><div class="item-header col-lg-4 br-lt"><img class="icon" src="<?= the_field('icone'); ?>" alt=""> <span class="title col-8"> <?= the_title(); ?></span></div><img class="detail left" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail.png" alt=""><div class="item-body col-lg-8 br-br"><span class="title"><?= the_title(); ?></span><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn right">Leia mais ></a></div></div> <?php $contador = 2; ?> <?php elseif ($contador == 2) : ?> <div class="item col-12 flex-lg-row-reverse px-0"><div class="item-header col-lg-4 br-rt"><img class="icon" src="<?= the_field('icone'); ?>" alt=""> <span class="title"><?= the_title(); ?></span></div><img class="detail right" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail.png" alt=""><div class="item-body col-lg-8 br-bl"><span class="title"><?= the_title(); ?></span><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn">Leia mais ></a></div></div> <?php $contador = 1; ?> <?php endif; ?> <?php endwhile;
    endif; ?> </div></section> <?= get_template_part('porque'); ?> <?= get_template_part('pre-footer'); ?> <?= get_footer(); ?>