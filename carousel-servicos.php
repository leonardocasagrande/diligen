<div class="servicos-home"><span class="title-section">Serviços</span><div class="wrapper"><div class="carousel-servicos"><div class="item br-lt"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/service1.png" alt=""> <span class="title">Assessoria a Administradores Judiciais</span><p>Suporte econômico-financeiro a Administradores Judiciais.</p><a href="<?= get_site_url(); ?>/servicos/administracao-judicial" class="btn">Saiba mais ></a></div><div class="item br-lt br-lg-none"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/service6.png" alt=""> <span class="title">Conciliação e Mediação</span><p>Busca de soluções para conflitos existentes.</p><a href="<?= get_site_url(); ?>/servicos/mediacao" class="btn">Saiba mais ></a></div><div class="item br-lt br-lg-rb"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/service5.png" alt=""> <span class="title">Due diligence e Valuation</span><p>Investigações e estudos financeiros para estimativa de valor de ativos.</p><a href="<?= get_site_url(); ?>/servicos/valuation" class="btn">Saiba mais ></a></div><!-- <div class="item   br-lt br-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/service4.png" alt="">

        <span class="title">Gestão Judicial</span>

        <p> É o responsável pela gestão da empresa em recuperação judicial por determinação da instância decisória.</p>

        <a href="<?= get_site_url(); ?>/servico/gestao-judicial" class="btn">Saiba mais ></a>
      </div>

      <div class="item   br-lt br-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/service5.png" alt="">

        <span class="title">Valuation</span>

        <p>É o estudo que visa estimar e demonstrar, de forma fundamentada e clara, o valor de uma empresa.</p>

        <a href="<?= get_site_url(); ?>/servico/valuation" class="btn">Saiba mais ></a>
      </div>


      <div class="item   br-lt br-lg-rb">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/service6.png" alt="">

        <span class="title">Mediação</span>

        <p>É a atividade de buscar uma solução em um conflitos existentes entre as partes.</p>

        <a href="<?= get_site_url(); ?>/servico/mediacao" class="btn">Saiba mais ></a>
      </div> --></div></div></div>