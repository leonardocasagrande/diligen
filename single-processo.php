<?php get_header() ?> <main class="processo"><div class="container"><div class="row"><div class="col-lg-4 d-none d-lg-block"><div class="fundo-azul"> <?php
                    $link_logo = get_the_post_thumbnail_url();
                    if (!$link_logo) {
                        $link_logo = get_stylesheet_directory_uri() . "/dist/img/icone-padrao.png";
                    }
                    ?> <img src="<?= $link_logo ?>" alt=""></div></div><div class="col-lg-8"><div class="informacoes-processos"><b>Processo:</b>&nbsp;<br class="d-lg-none"><?php the_field('numero_do_processo') ?> <br><b>Pedido:</b>&nbsp;<br class="d-lg-none"><?php the_field('numero_do_pedido') ?> <br><b>Comarca:</b>&nbsp;<br class="d-lg-none"><?php the_field('comarca') ?> <br><b>Vara:</b>&nbsp;<br class="d-lg-none"><?php the_field('vara') ?> <br><b>Juiz de direito:</b>&nbsp;<br class="d-lg-none"><?php the_field('juiz_de_direito') ?> <br><br><b>Próximos eventos:</b>&nbsp;<br class="d-lg-none"><?php the_field('proximos_eventos') ?> </div></div></div><div class="informacoes-acordeao d-lg-none"><div class="accordion" id="accordionExample"><div class="card"><div class="card-header" id="headingOne"><h2 class="mb-0"><button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span>Quadro de credores </span><i class="fas fa-plus"></i> <i class="fas fa-minus"></i></button></h2></div><div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample"><div class="card-body"> <?php the_field('conteudo_credores') ?> </div></div></div><div class="card"><div class="card-header" id="headingTwo"><h2 class="mb-0"><button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span>Planos de recuperação </span><i class="fas fa-plus"></i> <i class="fas fa-minus"></i></button></h2></div><div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample"><div class="card-body"> <?php
                            // Check rows exists.
                            if (have_rows('conteudo_planos')) :

                                // Loop through rows.
                                while (have_rows('conteudo_planos')) : the_row();
                            ?> <?php
                                    // Load sub field value.
                                    $nome = get_sub_field('nome_do_arquivo');
                                    $arquivo = get_sub_field('arquivo');
                                    // Do something...
                                    ?> <div class="arquivo"><h3><?= $nome; ?></h3><div class="download"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-download.png" alt="<?= $nome ?>"> <a href="<?= $arquivo ?>" class="cta-download" download>Download</a></div></div> <?php
                                // End loop.
                                endwhile;

                            // No value.
                            else :
                                // Do something...
                                echo "<span class='no-result'>Nenhum plano de recuperação foi publicado ainda</span>";
                            endif;
                            ?> </div></div></div><div class="card"><div class="card-header" id="headingThree"><h2 class="mb-0"><button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span>Relatórios mensais</span> <i class="fas fa-plus"></i> <i class="fas fa-minus"></i></button></h2></div><div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample"><div class="card-body"> <?php
                            // Check rows exists.
                            if (have_rows('conteudo_relatorios')) :

                                // Loop through rows.
                                while (have_rows('conteudo_relatorios')) : the_row();
                            ?> <?php
                                    // Load sub field value.
                                    $nome_relatorio = get_sub_field('nome_do_relatorio');
                                    $relatorio = get_sub_field('relatorio');
                                    // Do something...
                                    ?> <div class="arquivo relatorio"><h3><?= $nome_relatorio; ?></h3><div class="download"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-download-azul.png" alt="<?= $nome_relatorio ?>"> <a href="<?= $relatorio ?>" class="cta-download" download>Download</a></div></div> <?php
                                // End loop.
                                endwhile;

                            // No value.
                            else :
                                // Do something...
                                echo "<span class='no-result'>Nenhum relatório mensal foi publicado ainda</span>";
                            endif;
                            ?> </div></div></div><div class="card"><div class="card-header" id="headingOne"><h2 class="mb-0"><button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseQuatro" aria-expanded="true" aria-controls="collapseQuatro"><span>Assembleias</span> <i class="fas fa-plus"></i> <i class="fas fa-minus"></i></button></h2></div><div id="collapseQuatro" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample"><div class="card-body"> <?php the_field('conteudo_assembleias') ?> </div></div></div></div></div><div class="informacoes-tabs d-none d-lg-block"><ul class="nav nav-tabs nav-fill" id="myTab" role="tablist"><li class="nav-item" role="presentation"><a class="nav-link active" id="credores-tab" data-toggle="tab" href="#credores" role="tab" aria-controls="credores" aria-selected="true">Quadro de credores</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="planos-tab" data-toggle="tab" href="#planos" role="tab" aria-controls="planos" aria-selected="false">Planos de recuperação</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="relatorios-tab" data-toggle="tab" href="#relatorios" role="tab" aria-controls="relatorios" aria-selected="false">Relatórios mensais</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="assembleias-tab" data-toggle="tab" href="#assembleias" role="tab" aria-controls="assembleias" aria-selected="false">Assembleias</a></li></ul><div class="tab-content" id="myTabContent"><div class="tab-pane fade show active" id="credores" role="tabpanel" aria-labelledby="credores-tab"> <?php the_field('conteudo_credores') ?> </div><div class="tab-pane fade" id="planos" role="tabpanel" aria-labelledby="planos-tab"><div class="row align-items-start"> <?php
                        // Check rows exists.
                        if (have_rows('conteudo_planos')) :
                            $count = 1;
                            $total = count(get_field('conteudo_planos'));
                            $num_row = ceil($total / 3);
                            // var_dump($last_row);
                            $pocisao = "";
                            // Loop through rows.
                            while (have_rows('conteudo_planos')) : the_row();
                                $cur_row = ceil($count / 3);
                                // var_dump($cur_row);
                                if ($count == 3) {
                                    $pocisao = "top-right";
                                } elseif ($cur_row == $num_row && $count % 3 == 1) {
                                    $pocisao = "bottom-left";
                                } else {
                                    $pocisao = "";
                                }
                        ?> <?php
                                // Load sub field value.
                                $nome = get_sub_field('nome_do_arquivo');
                                $arquivo = get_sub_field('arquivo');
                                // Do something...
                                ?> <div class="col-4"><div class="arquivo <?= $pocisao ?>"><div class="separar"><h3><?= $nome; ?></h3><div class="download"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-download.png" alt="<?= $nome ?>"> <a href="<?= $arquivo ?>" class="cta-download" download>Download</a></div></div></div></div> <?php
                                $count++;
                            // End loop.
                            endwhile;

                        // No value.
                        else :
                            // Do something...
                            echo "<span class='no-result'>Nenhum plano de recuperação foi publicado ainda</span>";
                        endif;
                        ?> </div></div><div class="tab-pane fade" id="relatorios" role="tabpanel" aria-labelledby="relatorios-tab"><div class="row align-items-start"> <?php
                        // Check rows exists.
                        if (have_rows('conteudo_relatorios')) :
                            $count = 1;
                            $total = count(get_field('conteudo_relatorios'));
                            $num_row = ceil($total / 3);
                            // var_dump($last_row);
                            $pocisao = "";
                            // Loop through rows.
                            while (have_rows('conteudo_relatorios')) : the_row();
                                $cur_row = ceil($count / 3);
                                // var_dump($cur_row);
                                if ($count == 3) {
                                    $pocisao = "top-right";
                                } elseif ($cur_row == $num_row && $count % 3 == 1) {
                                    $pocisao = "bottom-left";
                                } else {
                                    $pocisao = "";
                                }
                        ?> <?php
                                // Load sub field value.
                                $nome_relatorio = get_sub_field('nome_do_relatorio');
                                $relatorio = get_sub_field('relatorio');
                                // Do something...
                                ?> <div class="col-4"><div class="arquivo relatorio <?= $pocisao ?>"><div class="separar"><h3><?= $nome_relatorio; ?></h3><div class="download"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-download-azul.png" alt="<?= $nome_relatorio ?>"> <a href="<?= $relatorio ?>" class="cta-download" download>Download</a></div></div></div></div> <?php
                            // End loop.
                            endwhile;

                        // No value.
                        else :
                            // Do something...
                            echo "<span class='no-result'>Nenhum relatório mensal foi publicado ainda</span>";
                        endif;
                        ?> </div></div><div class="tab-pane fade" id="assembleias" role="tabpanel" aria-labelledby="assembleias-tab"> <?php the_field('conteudo_assembleias') ?> </div></div></div><div class="link-processos d-lg-none"><a class="cta-processos" href="<?= get_site_url(); ?>/processos"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/pasta.png" alt="icone de pasta"> <span>Confira todos os<br>nossos processos</span> <i class="fas fa-2x fa-chevron-right"></i></a></div><div class="link-processos d-none d-lg-block"><a class="cta-processos" href="<?= get_site_url(); ?>/processos"><span class="bg-detalhe"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/pasta-azul.png" alt="icone de pasta"></span><span>Confira todos os nossos processos <i class="fas fa-lg fa-chevron-right"></i></span></a></div><div class="formulario-processos d-none d-lg-block"><h2>Envio de Documentos para o processo<br><?php the_title() ?></h2> <?php
            $shortcode = get_field('formulario_do_processo');
            echo do_shortcode($shortcode);
            ?> </div><button class="cta-formulario d-lg-none" data-toggle="modal" data-target="#exampleModal">Envio de Documentos<br>para o processo</button></div></main><div class="modal fade formulario-processos" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button></div><div class="modal-body"><h2>Envio de Documentos para o processo<br><?php the_title() ?></h2> <?php
                $shortcode = get_field('formulario_do_processo');
                echo do_shortcode($shortcode);
                ?> </div></div></div></div> <?php get_footer() ?>